#include <iostream>
#include "functions.h"

using namespace std;

//�������� �� �������� ����� 2^256
bool RangeForNumber(char a[], int n)
{
    int i;
    const int nt =85;
	char t[85]={1,1,5,7,9,2,0,9,0,0,};//C������� ������� ����� 2^256
	if (a[0]=='-')
	{
		if(n<=nt) return true;
		if(n>86) return false;
		int neg=1;
		for (i=0;i<n-1;i++)
		{
			if (a[i+1]>t[i]) return false;
			else
			if (t[i]!=a[i+1]) neg=0; 
		}
		if(neg==1) return true;
	}
	if (nt>n) return true;
	if(nt==n && a[0]!='-')
	{
		int pos=1;
		for (i=0;i<n;i++)
		{
			if (a[i]>t[i]) return false;
			else 
			if (t[i]!=a[i]) pos=0;
		}
		if(pos==1) return true;
	}
	return false;
}


//�������� �� ���� ����������� ��������
bool isNumber (char *str, int size)
{
   for (int i = 0; i < size; i++)
   {
	  if(i==0 && (int)str[i]==45)
			continue;
        if (str[i] <'0' || str[i]>'9')
			return false;
    }
	return true;
}
//���� �������� �����
void InputNumbers(char *ar, int &n)
{
 	int i=0;
	bool l=true;
	bool u=true;
	while(true)
	{
		int i=0, j=0,r1=0;
		cout << "\n ������� �����: ";
		ar[i]=getchar();
		while(ar[i]!='\n')
		{
			i++;
			ar[i]=getchar();	
		}
		n=i;
		l= isNumber(ar,n);
		for(i=0; i<n; i++)
		{
			if (i==0 && ar[i]=='-')
			{
				ar[i]='-';
			}
			else ar[i]-=48;
		}
		u= RangeForNumber(ar,n);
		if (!l|| !u) 
			cout <<"\n ������������ ��������. ��������� ����. \n";
		else break;
	}
}

//����������� �����
bool DefinitionSign (char b[], int n, char c[], int m)
{
	//���� ��� ����� "-"
	if(b[0]=='-' && c[0]=='-')
	{
		b[0]=b[0]-45;
		c[0]=c[0]-45;
		return true;
	}
	//���� ������ ����� "-", � ������ �� ����
	if(b[0]=='-' && c[0]!=0)
	{
		b[0]=b[0]-45;
		return false;
	}
	//���� ������ ����� "-",� ������ �� ����
	if(c[0]=='-' && b[0]!=0)
	{
		c[0]=c[0]-45;
		return false;
	}
	return true;
}

//����� �������� �����
void OutputNumbers(char *ar, int n)
{
    int i;
	if(ar[0]=='-')
	{
		for(i = 1; i < n; i++)
			printf("%d", ar[i]);
	}
	else 
		{
			for(i = 0; i < n; i++)
				printf("%d", ar[i]);
		}
}

//��������� ������� �����
void Multiplication(char b[], int n, char c[], int m, char e[], int &y)
{
	char h[200];
    int i,j,l,o;
	y=0;
	bool neg=false;
	neg=DefinitionSign(b,n,c,m);
    //�������� ���������
	for(i=0; i<n+m; i++)
		 h[i]=0;
    //���������
    for(i=0; i<m; i++)
	{
		o=0; l=0;
		for(j=n-1; j>=0; j--) 
		{
 			h[j+m-i]+=(b[j]*c[m-1-i] + o)%10;
			if(h[j+m-i]>9) 
			{
				l=h[j+m-i]/10;
				h[j+m-i]=h[j+m-i]%10;
			}
			o=(b[j]*c[m-i-1] + o)/10+l;        
			if(j==0) 
				h[j+m-1-i]+=o;
			l=0;
		}
    }
    //����������� �� ������� �����
    n=n+m;
    while(h[0]==0)
	{
		for(j=0; j<n-1; j++)
		{
			h[j]=h[j+1];
		}
		if(n!=1) n--; 
		else break;
    }
	y=n;
	//���� ���� �� ���������� "-"
	if(!neg)
	{
		e[0]='-';
		for(i=0; i<y; i++)
		{
			e[i+1] = h[i];
		}
		y=y+1;
	}
	else
    //����� ����������
    for(i=0; i<y; i++)
		e[i] = h[i];
}
//�������������� � ��� string
string IsCHARtoSTRING(char *e, int y)
{
	char *temp;
	string t;
	int w;
	temp = new char[y];
	if(e[0]=='-')
	{
		for(w = 0; w < y-1; ++w) 
			temp[w] = e[w+1] + 48;
		y=y-1;
	}
	else
		{
			for(w = 0; w < y; ++w) 
				temp[w] = e[w] + 48;
		}
	t = string(temp, y);
	return t;
}

//������� � ������� ��������� �� ��������� 18
string into18system(string a)
{
	string rez = a;	
	string result;	
	bool negNumber = false;
	if (a[0] == '-')
	{
		negNumber = true;
		rez.erase(rez.begin());
	}
	unsigned int i = 0;
	int ost = 0;		
	string div;						
	while (!rez.empty())
	{
		for (i = 0; i < rez.size(); i++)
		{
			ost = ost * 10 + (int)rez[i] - 48;
			
			if (ost < 18)
			{
				if (div.size() != 0)
				{
					div.append("0");
				}
			}
			else
			{
				div = div + (char)(ost / 18 + 48);
				ost = ost % 18;
			}
		}
		if (ost > 9)
		{
			result = (char)(ost + 55) + result;
		}
		else
		{
			result = (char)(ost + 48) + result;
		}
		rez = div;
		div.erase();
		ost = 0;
	}
	if (negNumber)
	{
		result = "-" + result;
	}
	return (result);
}

//������� � 35-������ ������� ���������
string into35system(string a)
{
	string rez = a;	
	string result;	
	bool negNumber = false;
	if (a[0] == '-')
	{
		negNumber = true;
		rez.erase(rez.begin());
	}
	unsigned int i = 0;
	int ost = 0;		
	string div;						
	while (!rez.empty())
	{
		for (i = 0; i < rez.size(); i++)
		{
			ost = ost * 10 + (int)rez[i] - 48;
			
			if (ost < 35)
			{
				if (div.size() != 0)
				{
					div.append("0");
				}
			}
			else
			{
				div = div + (char)(ost / 35 + 48);
				ost = ost % 35;
			}
		}
		if (ost > 9)
		{
			result = (char)(ost + 55) + result;
		}
		else
		{
			result = (char)(ost + 48) + result;
		}
		rez = div;
		div.erase();
		ost = 0;
	}
	if (negNumber)
	{
		result = "-" + result;
	}
	return (result);
}