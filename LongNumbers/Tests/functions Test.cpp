#include "functions.h"
#include <boost/test/unit_test.hpp>

//*************************����� �� ���� �����*****************************//
//�������� ������� � �����
BOOST_AUTO_TEST_CASE(Symbol_in_Number)
{
	char actual[5]={'5','a','2','2','3'};
	bool truth=false;
	BOOST_CHECK(isNumber(actual,5) == truth);
}
//�������� �� ���� �����
BOOST_AUTO_TEST_CASE(isNumbers)
{
	char actual[5]={'5','1','1','1','2'};
	bool truth=true;
	BOOST_CHECK(isNumber(actual,5) == truth);
}
//������ ������ "-"
BOOST_AUTO_TEST_CASE(FirstCharacter_is_Negative)
{
	char actual[5]={'-','1','1','1','1'};
	bool truth=true;
	BOOST_CHECK(isNumber(actual,5) == truth);
}
//������ ������ �� "-"
BOOST_AUTO_TEST_CASE(FirstCharacter_is_no_Negative)
{
	char actual[5]={'+','1','1','1','1'};
	bool truth=false;
	BOOST_CHECK(isNumber(actual,5) == truth);
}
//�����������, ��� ����� ��������� � ��������� [-2^256; 2^256]
BOOST_AUTO_TEST_CASE(Number_is_range)
{
	char actual[84]={'-',8,8,8,8,8,8,8,};
	bool truth=true;
	BOOST_CHECK(RangeForNumber(actual,84) == truth);
}
//����� ������� �� ��������
BOOST_AUTO_TEST_CASE(NamberMore)
{
	char actual[100]={1,0,};
	bool truth=false;
	BOOST_CHECK(RangeForNumber(actual,100) == truth);
}
//����� ����� 2^256
BOOST_AUTO_TEST_CASE(EqualityPosNumber)
{
	char actual[85]={1,1,5,7,9,2,0,9,};
	bool truth=true;
	BOOST_CHECK(RangeForNumber(actual,85) == truth);
}
//����� ����� -2^256
BOOST_AUTO_TEST_CASE(EqualityNegNumber)
{
	char actual[86]={'-',1,1,5,7,9,2,0,9,};
	bool truth=true;
	BOOST_CHECK(RangeForNumber(actual,86) == truth);
}
//�����������, ���� ����� �� ����� �������������� "+" ��������� (>2^256)
BOOST_AUTO_TEST_CASE(NumberNotEqualpositiveRange)
{
	char actual[85]={1,1,5,7,9,2,0,9,8,};
	bool truth=false;
	BOOST_CHECK(RangeForNumber(actual,85) == truth);
}
//�����������, ���� ����� �� ����� �������������� "-" ��������� (>-2^256)
BOOST_AUTO_TEST_CASE(NumberNotEqualnegativeRange)
{
	char actual[86]={'-',1,1,5,7,9,2,0,9,8,};
	bool truth=false;
	BOOST_CHECK(RangeForNumber(actual,86) == truth);
}
//*****************************************************************************************//


//******************************����� �� ��������� ������������� �����***************************************//
//���� �� ��������� ��������� ����� �� ��������
BOOST_AUTO_TEST_CASE(MultiplicationShortNumbers)
{
	char actual1[1]={8};
	char actual2[1]={5};
	char result[2]={4,0};
	char actual3[2];
	int y;
	Multiplication(actual1,1,actual2,1,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}
//���� �� ��������� �������� ����� �� ��������
BOOST_AUTO_TEST_CASE(Multiplication_Long_to_short)
{
	char actual1[23]={1,2,3,4,5,2,1,3,2,1,5,4,6,4,5,4,5,6,4,5,6,4,6};
	char actual2[1]={5};
	char result[23]={6,1,7,2,6,0,6,6,0,7,7,3,2,2,7,2,8,2,2,8,2,3,0};
	char actual3[23];
	int y;
	Multiplication(actual1,23,actual2,1,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}

//���� �� ��������� ������� �����
BOOST_AUTO_TEST_CASE(MultiplicationLongNumbers)
{
	char actual1[15]={2,2,2,2,2,2,2,2,2,2,2,2,2,2,2};
	char actual2[15]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	char result[30]={2,4,6,9,1,3,5,8,0,2,4,6,9,1,3,0,8,6,4,1,9,7,5,3,0,8,6,4,2};
	char actual3[1000];
	int y;
	Multiplication(actual1,15,actual2,15,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}

//���� �� ��������� c ��������� ����� �����
BOOST_AUTO_TEST_CASE(MultiplicationDifferentlength)
{
	char actual1[10]={2,2,2,2,2,2,2,2,2,2};
	char actual2[15]={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	char result[24]={2,4,6,9,1,3,5,8,0,2,2,2,2,2,1,9,7,5,3,0,8,6,4,2};
	char actual3[100];
	int y;
	Multiplication(actual1,10,actual2,15,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}
//���� ���� ��� ��������� ����
BOOST_AUTO_TEST_CASE(MultiplicationTwoNumbersZero)
{
	char actual1[1]={0};
	char actual2[1]={0};
	char result[1]={0};
	char actual3[5];
	int y;
	Multiplication(actual1,1,actual2,1,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}
//***********************************************************************************//


//****************************����� �� ������� � 18 ��********************************//
BOOST_AUTO_TEST_CASE(Number_4_to_18)
{
	string actual("4");
	BOOST_CHECK_EQUAL(into18system(actual),"4");
}
//������� ����� 20 � 18 ������� ���������
BOOST_AUTO_TEST_CASE(Number_20_to_18)
{
	string actual("20");
	BOOST_CHECK_EQUAL(into18system(actual),"12");
}
//������� ����� � �������������� ����
BOOST_AUTO_TEST_CASE(Number_33_to_18)
{
	string actual("33");
	BOOST_CHECK_EQUAL(into18system(actual),"1F");
}

//������� ����� 456 � 18 ������� ���������
BOOST_AUTO_TEST_CASE(Number_456_to_18)
{
	string actual("456");
	BOOST_CHECK_EQUAL(into18system(actual),"176");
}
//������� �������� ����� � 18 ������� ���������

BOOST_AUTO_TEST_CASE(LongNumber_to_18)
{
	string actual("123456789123456789");
	BOOST_CHECK_EQUAL(into18system(actual),"5GCH3D8H8061C9");
}
//*************************************************************************************//

//**************************����� �� ��������� ������������� �����***************************//

//���� �� ��������� ���� ������ ������������� �����
BOOST_AUTO_TEST_CASE(firstNegativeNumber)
{
	char actual1[5]={'-',2,3,6,5};
	char actual2[2]={6,1};
	char result[7]={'-',1,4,4,2,6,5};
	char actual3[20];
	int y;
	Multiplication(actual1,5,actual2,2,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}

///���� �� ��������� ���� ��� ������������� �����
BOOST_AUTO_TEST_CASE(twoNegativeNumbers)
{
	char actual1[4]={'-',2,2,2};
	char actual2[3]={'-',5,0};
	char result[5]={1,1,1,0,0};
	char actual3[20];
	int y;
	Multiplication(actual1,4,actual2,3,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}
//���� �� ��������� ���� ����� ���� � �������� ������� �����
BOOST_AUTO_TEST_CASE(MulltwoNegativeNumber_to_zero)
{
	char actual1[1]={0};
	char actual2[3]={'-',2,3};
	char result[1]={0};
	char actual3[5];
	int y;
	Multiplication(actual1,1,actual2,3,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}
//���� �� ��������� ���� ���� � �������� ������� �����
BOOST_AUTO_TEST_CASE(MullfirstNegativeNumbers_to_zero)
{
	char actual1[17]={'-',5,5,5,4,5,4,5,4,5,4,5,4,5,4,5,4};
	char actual2[1]={0};
	char result[1]={0};
	char actual3[5];
	int y;
	Multiplication(actual1,17,actual2,1,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}

//���� �� ��������� ���� ������ ����� �������������
BOOST_AUTO_TEST_CASE(twoNegativeNumber)
{
	char actual1[2]={1,2};
	char actual2[7]={'-',2,2,2,2,2,2};
	char result[8]={'-',2,6,6,6,6,6,4};
	char actual3[30];
	int y;
	Multiplication(actual1,2,actual2,7,actual3,y);
	BOOST_CHECK_EQUAL(actual3,result);
}

//****************************************************************************************//
 
//****************����� �� ����� �������������� ����� � 18 ������� ���������**************//

BOOST_AUTO_TEST_CASE(NegativeNumber_to_18system)
{
	string actual("-123456");
	BOOST_CHECK_EQUAL(into18system(actual),"-1330C");
}

//****************************************************************************************//

//******************����� �� ������� � 35-������ ������� ���������**************************//

//����������� ������� ����� � 35-������ � ��������� � ������� ������
BOOST_AUTO_TEST_CASE(Number36to35base)
{

	string actual("36");
	BOOST_CHECK_EQUAL(into35system(actual),"11");
}
//����������� �����, ������� ������ ��������� 35-������ �������
BOOST_AUTO_TEST_CASE(Number20to35base)
{
	string actual("4");
	BOOST_CHECK_EQUAL(into35system(actual),"4");
}
//����������� ������� ����� �������� 35
BOOST_AUTO_TEST_CASE(Number105to35base)
{

	string actual("105");
	BOOST_CHECK_EQUAL(into35system(actual),"30");
}
//����������� ������� ����� � ����������� ����
BOOST_AUTO_TEST_CASE(Number80to35base)
{

	string actual("80");
	BOOST_CHECK_EQUAL(into35system(actual),"2A");
}

//������� ������������� ����� � 35-������ ��

BOOST_AUTO_TEST_CASE(negNumberto35base)
{
	string actual("-35");
	BOOST_CHECK_EQUAL(into35system(actual),"-10");
}






